package sunrise.cps.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sunrise.cps.domain.entities.user.ApiUserEntity;
import sunrise.cps.domain.entities.user.ApiUserRepository;
import sunrise.cps.exception.CpsApiException;
import sunrise.cps.exception.common.ApiErrors;

@Service
public class ApiUserServiceImpl implements ApiUserService {
    @Autowired
    private ApiUserRepository apiUserRepository;

    @Override
    public ApiUserEntity getApiUserDetails(String apiUser) {
        return apiUserRepository.findByUsername(apiUser)
                .orElseThrow(
                        () -> new CpsApiException(ApiErrors.USER_NOT_FOUND, "User not found : " + apiUser));
    }



}
