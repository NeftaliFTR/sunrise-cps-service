package sunrise.cps.service.user;

import sunrise.cps.domain.entities.user.ApiUserEntity;

public interface ApiUserService {

    ApiUserEntity getApiUserDetails(String apiUserName);
}
