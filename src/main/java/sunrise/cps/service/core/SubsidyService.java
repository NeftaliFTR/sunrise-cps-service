package sunrise.cps.service.core;

import sunrise.cps.api.model.Subsidy;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import sunrise.cps.api.model.BalanceDetailsResponse;

public interface SubsidyService {
    Subsidy getBucketDetails(String accountNumber);

    BalanceDetailsResponse getBalanceDetails(String accountNumber, String siteId, String balanceType);

}
