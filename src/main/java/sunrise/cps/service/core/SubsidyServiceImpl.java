package sunrise.cps.service.core;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sunrise.cps.api.model.*;
import sunrise.cps.domain.entities.account.AccountEntity;
import sunrise.cps.domain.entities.account.AccountRepository;
import sunrise.cps.domain.entities.bucket.BucketEntity;
import sunrise.cps.domain.entities.bucket.BucketRepository;
import sunrise.cps.domain.entities.sitelimit.SiteLimitEntity;
import sunrise.cps.domain.entities.sitelimit.SiteLimitRepository;
import sunrise.cps.exception.CpsApiException;
import sunrise.cps.exception.common.ApiErrors;
import sunrise.cps.exception.common.AssertUtil;
import sunrise.cps.util.core.SubsidyMapper;
import sunrise.cps.util.core.SubsidyUtil;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;


@Service
@Slf4j
public class SubsidyServiceImpl implements SubsidyService {
    @Autowired
    BucketRepository bucketRepository;
    @Autowired
    SiteLimitRepository siteLimitRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    SubsidyMapper subsidyMapper;
    @Autowired
    SubsidyUtil subsidyUtil;

    @Override
    @Transactional
    public Subsidy getBucketDetails(String accountNumber) {
        Optional<BucketEntity> bucketEntityOptional = bucketRepository.findActiveBucketDetailsByAccountNumber(accountNumber);
        if (bucketEntityOptional.isPresent()) {
            BucketEntity bucketEntity = bucketEntityOptional.get();
            SubsidyMapper subsidyMapper = new SubsidyMapper();
            return subsidyMapper.mapSubsidy(bucketEntity);
        }
        return null;
    }

    @Transactional
    @Override
    public BalanceDetailsResponse getBalanceDetails(String accountNumber, String siteId, String balanceType) {
        AssertUtil.notBlank((accountNumber), "accountNumber must be provided");
        AssertUtil.notBlank((siteId), "siteId must be provided");
        Optional<SiteLimitEntity> siteLimitEntity = siteLimitRepository.findBySiteId(siteId);
        Optional<AccountEntity> accountEntity = accountRepository.findByAccountNumber(accountNumber);
        BalanceBucketDetails balanceBucketDetails = null;
        try {
            if (siteLimitEntity.isPresent()) {
                BucketEntity bucketEntity = siteLimitEntity.get().getBucket();
                boolean validDate = subsidyUtil.compareDate(bucketEntity.getValidityStartDate(), bucketEntity.getValidityEndDate());
                if (validDate && accountNumber.equalsIgnoreCase(bucketEntity.getAccount().getAccountNumber())) {
                    balanceBucketDetails = new BalanceBucketDetails();
                    balanceBucketDetails.setRef(bucketEntity.getBucketRefId());
                    List<Balance> balanceList = new ArrayList<>();
                    if (StringUtils.isNotEmpty(balanceType)) {
                        Balance balance = subsidyMapper.mapBalancetoType(BalanceType.fromValue(balanceType), bucketEntity, siteLimitEntity.get().getAmountLimit());
                        balanceList.add(balance);
                    } else {
                        balanceList = subsidyMapper.calBalance(bucketEntity, siteLimitEntity.get().getAmountLimit());
                    }
                    balanceBucketDetails.setBalance(balanceList);
                } else {
                    log.warn("bucket is not in valid date range or input site map with account number ::  {}, {}, {}", bucketEntity.getValidityStartDate(), bucketEntity.getValidityEndDate(), bucketEntity.getAccount().getAccountNumber());
                }
            } else if (accountEntity.isPresent()) {
                List<BucketEntity> bucketEntityList = accountEntity.get().getBuckets();
                Optional<BucketEntity> validBucketEntity = bucketEntityList
                        .stream()
                        .filter(bucketEntity -> subsidyUtil.compareDate(bucketEntity.getValidityStartDate(), bucketEntity.getValidityEndDate()))
                        .findFirst();
                if (validBucketEntity.isPresent()) {
                    balanceBucketDetails = new BalanceBucketDetails();
                    balanceBucketDetails.setRef(validBucketEntity.get().getBucketRefId());
                    List<Balance> balanceList = new ArrayList<>();
                    if (StringUtils.isNotEmpty(balanceType)) {
                        Balance balance = subsidyMapper.mapBalancetoType(BalanceType.fromValue(balanceType), validBucketEntity.get(), null);
                        balanceList.add(balance);
                    } else {
                        balanceList = subsidyMapper.calBalance(validBucketEntity.get(), null);
                    }
                    balanceBucketDetails.setBalance(balanceList);
                } else {
                    log.warn("No bucket found in valid date range for account number ::  {} ", accountNumber);
                }
            } else {
                log.warn("account number not found ::  {} ", accountNumber);
            }
            return Objects.nonNull(balanceBucketDetails) ? new BalanceDetailsResponse().balanceBucketDetails(balanceBucketDetails) : null;
        } catch (Exception e) {
            log.error("Error during getBalanceDetails {} by siteId ", siteId, e);
            throw new CpsApiException(ApiErrors.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
