package sunrise.cps.util.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class SubsidyUtil {
    public static boolean compareDate(Date validStartDate, Date validEndDate) {
        Date date = new Date();
        return ((validStartDate.before(date) )&& (validEndDate.after(date)));
    }
}
