package sunrise.cps.util.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import sunrise.cps.api.model.*;
import sunrise.cps.domain.entities.account.AccountEntity;
import sunrise.cps.domain.entities.bucket.BucketEntity;
import sunrise.cps.domain.entities.sitelimit.SiteLimitEntity;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
@Component
@Slf4j
public class SubsidyMapper {

    public static Subsidy mapSubsidy(BucketEntity bucketEntity) {
        if (Objects.isNull(bucketEntity)) {
            return null;
        }
        return new Subsidy()
                .account(mapAccountBucketDetails(bucketEntity))
                .sites(mapSiteBucketDetails(bucketEntity));
    }

    private static AccountBucketDetails mapAccountBucketDetails(BucketEntity bucketEntity) {
        return new AccountBucketDetails()
                .balance(calBalance(bucketEntity, null))
                .ref(bucketEntity.getBucketRefId())
                .mainSiteId(bucketEntity.getAccount().getMainSiteId())
                .identifier(bucketEntity.getAccount().getAccountNumber())
                .name(bucketEntity.getAccount().getAccountName())
                .startDate(LocalDate.parse(bucketEntity.getValidityStartDate().toString()))
                .endDate(LocalDate.parse(bucketEntity.getValidityEndDate().toString()));
    }

    public static List<Balance> calBalance(BucketEntity bucketEntity, Double amountLimit) {
        List<Balance> balanceList = new ArrayList<>();
        balanceList.add(mapBalancetoType(BalanceType.TOTAL, bucketEntity, null));
        balanceList.add(mapBalancetoType(BalanceType.USED, bucketEntity, null));
        balanceList.add(mapBalancetoType(BalanceType.AVAIL, bucketEntity, amountLimit));
        return balanceList;
    }

    public static Balance mapBalancetoType(BalanceType balanceType, BucketEntity bucketEntity, Double amountLimit) {
        switch (balanceType) {
            case TOTAL:
                return new Balance().type(balanceType).amount(BigDecimal.valueOf(bucketEntity.getTotalAmount()));
            case USED:
                return new Balance().type(balanceType).amount(BigDecimal.valueOf(bucketEntity.getUsedAmount()));
            case AVAIL:
                if (Objects.nonNull(amountLimit)) {
                    BigDecimal availAmount = BigDecimal.valueOf(bucketEntity.getTotalAmount()).subtract(BigDecimal.valueOf(bucketEntity.getUsedAmount()));
                    if (availAmount.compareTo(BigDecimal.valueOf(amountLimit)) > 0) {
                        return new Balance().type(balanceType).amount(BigDecimal.valueOf(amountLimit));
                    }
                }
                return new Balance().type(balanceType).amount(BigDecimal.valueOf(bucketEntity.getTotalAmount()).subtract(BigDecimal.valueOf(bucketEntity.getUsedAmount())));
        }
        return null;
    }

    private static List<SiteBucketDetails> mapSiteBucketDetails(BucketEntity bucketEntity) {
        return bucketEntity.getSiteLimit().stream()
                .map(siteLimitEntity -> {
                    SiteBucketDetails siteBucketDetails = new SiteBucketDetails();
                    siteBucketDetails.identifier(siteLimitEntity.getSiteId());
                    siteBucketDetails.limit(BigDecimal.valueOf(siteLimitEntity.getAmountLimit()));
                    return siteBucketDetails;
                })
                .collect(Collectors.toList());
    }
}
