package sunrise.cps.util;

import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;
import sunrise.cps.config.ApplicationConstants;

import java.util.HashMap;
import java.util.Map;

public final class DefaultProfileUtil {

  private DefaultProfileUtil() {
  }

  public static void addDefaultProfile(SpringApplication app) {
    Map<String, Object> defProperties = new HashMap<>();
    defProperties.put(ApplicationConstants.SPRING_PROFILE_DEFAULT, ApplicationConstants.SPRING_PROFILE_DEVELOPMENT);
    app.setDefaultProperties(defProperties);
  }

  public static String[] getActiveProfiles(Environment env) {
    String[] profiles = env.getActiveProfiles();
    if (profiles.length == 0) {
      return env.getDefaultProfiles();
    }
    return profiles;
  }
}
