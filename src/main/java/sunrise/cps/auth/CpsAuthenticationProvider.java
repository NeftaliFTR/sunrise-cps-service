package sunrise.cps.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import sunrise.cps.adapter.LdapAdapter;
import sunrise.cps.domain.entities.user.ApiUserEntity;
import sunrise.cps.service.user.ApiUserService;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CpsAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    LdapAdapter ldapAdapter;
    @Autowired
    ApiUserService apiUserService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info("Authentication : {}", authentication);
        ApiUserEntity apiUser = apiUserService.getApiUserDetails(authentication.getName());
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (apiUser != null) {
            boolean checkApiUser = ldapAdapter.authorizeUser(authentication.getName(), (String) authentication.getCredentials());
            if (checkApiUser) {
                authorities.add(new SimpleGrantedAuthority("SUBSIDY"));
                return new UsernamePasswordAuthenticationToken(authentication.getName(), authentication.getCredentials(), authorities);
            }
        }
        return new UsernamePasswordAuthenticationToken(authentication.getName(), authentication.getCredentials(),authorities);

    }

    @Override
    public boolean supports(Class<? extends Object> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
