package sunrise.cps.domain.entities.bucket;

import lombok.*;
import sunrise.cps.domain.entities.BaseEntity;
import sunrise.cps.domain.entities.account.AccountEntity;
import sunrise.cps.domain.entities.dataprocess.DataProcessEntity;
import sunrise.cps.domain.entities.sitelimit.SiteLimitEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CPS_SUBSIDY_BUCKET")
//TODO: Optimistic lock need to require in this table
public class BucketEntity extends BaseEntity implements Serializable{

    @Column(name = "REF_ID")
    private String bucketRefId;

    @Column(name = "TOTAL_AMOUNT")
    private Double totalAmount;

    @Column(name = "VALIDITY_START_DATE")
    @Temporal(TemporalType.DATE)
    private Date validityStartDate;

    @Column(name = "VALIDITY_END_DATE")
    @Temporal(TemporalType.DATE)
    private Date validityEndDate;

    @Column(name = "USED_AMOUNT")
    private Double usedAmount;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private BucketStatus bucketStatus;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private AccountEntity account;

    @OneToMany(mappedBy = "bucket")
    private List<SiteLimitEntity> siteLimit;

    @OneToMany(mappedBy = "bucket")
    private List<DataProcessEntity> dataProcess;
}
