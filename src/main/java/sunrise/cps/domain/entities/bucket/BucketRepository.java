package sunrise.cps.domain.entities.bucket;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BucketRepository extends JpaRepository<BucketEntity,Long> {
    @Query("select s from BucketEntity s where s.account.accountNumber = :accountNumber AND s.bucketStatus = 'ACTIVE'")
    Optional<BucketEntity> findActiveBucketDetailsByAccountNumber(@Param("accountNumber")String accountNumber);
}

