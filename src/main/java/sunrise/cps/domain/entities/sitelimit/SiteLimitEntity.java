package sunrise.cps.domain.entities.sitelimit;

import lombok.*;
import sunrise.cps.domain.entities.BaseEntity;
import sunrise.cps.domain.entities.bucket.BucketEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CPS_SITES_LIMIT")
public class SiteLimitEntity extends BaseEntity implements Serializable{

    @Column(name = "SITE_ID")
    private String siteId;

    @Column(name = "AMOUNT_LIMIT")
    private Double amountLimit;

    @ManyToOne
    @JoinColumn(name = "BUCKET_REF_ID", referencedColumnName = "REF_ID")
    private BucketEntity bucket;
}
