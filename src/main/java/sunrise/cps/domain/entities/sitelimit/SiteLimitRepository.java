package sunrise.cps.domain.entities.sitelimit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SiteLimitRepository extends JpaRepository<SiteLimitEntity,Long> {
    Optional<SiteLimitEntity> findById(String id);

    Optional<SiteLimitEntity> findBySiteId(String siteId);
}