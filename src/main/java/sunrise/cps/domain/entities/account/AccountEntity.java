package sunrise.cps.domain.entities.account;

import lombok.*;
import sunrise.cps.domain.entities.BaseEntity;
import sunrise.cps.domain.entities.bucket.BucketEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CPS_ACCOUNTS")
public class AccountEntity extends BaseEntity implements Serializable{

    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "ACCOUNT_NAME")
    private String accountName;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "MAIN_SITE_ID")
    private String mainSiteId;

    @OneToMany(mappedBy = "account")
    private List<BucketEntity> buckets;
}
