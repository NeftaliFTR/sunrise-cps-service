package sunrise.cps.domain.entities.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sunrise.cps.common.Status;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity,Long>{
    Optional<AccountEntity> findByAccountNumber(String accountNumber);
}
