package sunrise.cps.domain.entities.dataprocess;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataProcessRepository extends JpaRepository<DataProcessEntity,Long> {

}

