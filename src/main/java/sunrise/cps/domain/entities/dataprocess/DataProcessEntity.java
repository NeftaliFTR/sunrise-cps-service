package sunrise.cps.domain.entities.dataprocess;

import lombok.*;
import sunrise.cps.domain.entities.BaseEntity;
import sunrise.cps.domain.entities.bucket.BucketEntity;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CPS_DATA_PROCESS")
public class DataProcessEntity extends BaseEntity implements Serializable{

    @Column(name = "PROCESS_ID")
    private String processId;

    @Column(name = "SITE_ID")
    private String siteId;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private ProcessStatus processStatus;

    @Column(name = "AMOUNT")
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "BUCKET_REF_ID", referencedColumnName = "REF_ID")
    private BucketEntity bucket;

}

