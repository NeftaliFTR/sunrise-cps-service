package sunrise.cps.domain.entities.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApiUserRepository extends JpaRepository<ApiUserEntity, Long> {

    Optional<ApiUserEntity> findByUsername(String username);

}
