package sunrise.cps.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;

@MappedSuperclass
@Getter
@Setter
public class BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  protected Long id;

  @Column(name = "CREATION_TIME")
  protected OffsetDateTime creationTime = OffsetDateTime.now();

  @Column(name = "CHANGE_TIME")
  protected OffsetDateTime changeTime;

  @PrePersist
  protected void onCreate() {
    creationTime = OffsetDateTime.now();
  }

}
