package sunrise.cps.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import sunrise.cps.common.CallerApplication;
import sunrise.cps.common.OriginatorApplication;
import sunrise.cps.common.RequestContext;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

/**
 * Filter to log headers and populate RequestContext
 */
@Slf4j
@Component("headers-filter")
public class HeadersFilter implements Filter {

  private final RequestContext requestContext;

  /* Sunrise standard */
  private static final String ORIG_REQUEST_ID_HEADER = "sunrise-orig-req-id";
  private static final String ORIG_CONTEXT_ID_HEADER = "sunrise-orig-ctx-id";
  private static final String ORIG_APP_HEADER = "sunrise-orig-app";
  private static final String ORIG_SENT_TIME_HEADER = "sunrise-orig-timestamp";
  private static final String ORIG_OPERATION_NAME_HEADER = "sunrise-orig-operation";
  private static final String ORIG_LOCATION_ID_HEADER = "sunrise-orig-location-id";
  private static final String ORIG_IP_ADDRESS_HEADER = "sunrise-orig-ip";
  private static final String CALLER_REQUEST_ID_HEADER = "sunrise-caller-req-id";
  private static final String CALLER_APP_HEADER = "sunrise-caller-app";
  private static final String CALLER_SENT_TIME_HEADER = "sunrise-caller_timestamp";

  /* SBA specific */
  private static final String LOGGED_IN_USER_HEADER = "sunrise-logged-in-user";
  private static final String LOGGED_IN_ALIAS_HEADER = "sunrise-logged-in-user-alias";

  /* SBA UI related*/
  private static final String X_CORRELATION_ID = "x-correlation-id";

  private static final String ENCODING = "UTF-8";

  private static final String X_SUNRISE_CHANNEL = "x-sunrise-channel";

  //private static final String DEFAULT_SUNRISE_CHANNEL = "SBP";

  private static final List<String> excludedURLS = Arrays.asList(
      "/api/swagger",
      "/api/webjars"
  );

  public HeadersFilter(RequestContext requestContext) {
    this.requestContext = requestContext;
  }


  @Override
  public void init(FilterConfig arg) {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    try {
      //logHttpDetails(httpRequest, httpResponse);
      enchanceMDC(httpRequest);
      populateRequestContext(httpRequest);
      chain.doFilter(request, response);
    } finally {
      // if for some reason this filter is skipped, clear requestContext
      this.requestContext.clear();
    }
  }

  @Override
  public void destroy() {
  }


  private void logHttpDetails(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
    if (isExcludedUrl(httpRequest.getRequestURI())) {
      return;
    }
    StringBuilder buf = new StringBuilder(
        "\n# REQUEST with HTTP status code: " + httpResponse.getStatus() + "\n");
    buf.append("# ----------------------------------\n");

    buf.append("# ").append(httpRequest.getMethod()).append(" ").append(httpRequest.getRequestURI())
        .append("\n");

    if (httpRequest.getQueryString() != null) {
      buf.append("# Query: ").append(httpRequest.getQueryString()).append("\n");
    }

    buf.append("# ----------------------------------\n");
    buf.append("# ").append(httpRequest.getRemoteAddr()).append(":")
        .append(httpRequest.getRemotePort());
    buf.append("# ").append(httpRequest.getRemoteHost()).append("\n");
    buf.append("# ").append(httpRequest.getLocalAddr()).append(":")
        .append(httpRequest.getLocalPort());
    buf.append("# ").append(httpRequest.getLocalName()).append("\n");
    buf.append("# ").append(httpRequest.getServerName()).append(":")
        .append(httpRequest.getServerPort()).append("\n");
    buf.append("# ----------------------------------\n");

    buf.append(getParams(httpRequest));
    buf.append(getHeaders(httpRequest));
    buf.append(getHeaders(httpResponse));

    log.info(buf.toString());
  }

  private void populateRequestContext(HttpServletRequest httpRequest) {
    String origAppHeader = httpRequest.getHeader(ORIG_APP_HEADER);
    String callerAppHeader = httpRequest.getHeader(CALLER_APP_HEADER);
    String channel = httpRequest.getHeader(X_SUNRISE_CHANNEL);
    requestContext.setOrigRequestId(
        httpRequest.getHeader(ORIG_REQUEST_ID_HEADER)); // use X-Request-ID  if present
    requestContext.setOrigContextId(httpRequest.getHeader(ORIG_CONTEXT_ID_HEADER));
    requestContext.setOrigApp(
        StringUtils.isNotBlank(origAppHeader) ? OriginatorApplication.valueOf(origAppHeader)
            : null);
    requestContext.setOrigIPAddress(httpRequest.getHeader(ORIG_IP_ADDRESS_HEADER));
    requestContext.setOrigOperationName(httpRequest.getHeader(ORIG_OPERATION_NAME_HEADER));
    requestContext.setOrigSentTime(httpRequest.getHeader(ORIG_SENT_TIME_HEADER));
    requestContext.setOrigLocationId(httpRequest.getHeader(ORIG_LOCATION_ID_HEADER));
    requestContext.setCallerRequestId(httpRequest.getHeader(CALLER_REQUEST_ID_HEADER));
    requestContext.setCallerApp(
        StringUtils.isNotBlank(callerAppHeader) ? CallerApplication.valueOf(callerAppHeader)
            : null);
    requestContext.setCallerSentTime(httpRequest.getHeader(CALLER_SENT_TIME_HEADER));
    requestContext.setLoggedInUser(httpRequest.getHeader(LOGGED_IN_USER_HEADER));
    requestContext.setLoggedInAlias(httpRequest.getHeader(LOGGED_IN_ALIAS_HEADER));
    requestContext.setChannel(StringUtils.isNotBlank(channel) ? channel : null);
  }

  private String getParams(HttpServletRequest req) {
    StringBuilder buf = new StringBuilder();

    if (!req.getParameterMap().isEmpty()) {
      buf.append("# PARAMS:\n");

      for (String key : req.getParameterMap().keySet()) {
        String[] values = req.getParameterMap().get(key);
        buf.append("#  --- ").append(key).append(" = ").append(Arrays.toString(values));

        buf.append("\n");
      }
    }

    return buf.toString();
  }

  private String getHeaders(HttpServletRequest req) {
    StringBuilder buf = new StringBuilder("# IN-HEADERS:\n");

    Enumeration<String> names = req.getHeaderNames();

    while (names.hasMoreElements()) {
      String name = names.nextElement();
      buf.append("#  --- ").append(name).append(" = ").append(req.getHeader(name)).append("\n");
    }

    return buf.toString();
  }

  private String getHeaders(HttpServletResponse res) {
    StringBuilder buf = new StringBuilder();

    Collection<String> names = res.getHeaderNames();

    if (!names.isEmpty()) {
      buf.append("# OUT-HEADERS:\n");

      for (String name : names) {
        buf.append("#  --- ").append(name).append(" = ").append(res.getHeader(name)).append("\n");
      }
    }

    return buf.toString();
  }

  private boolean isExcludedUrl(String requestURI) {
    return excludedURLS
        .stream()
        .anyMatch(requestURI::contains);
  }

  private void enchanceMDC(HttpServletRequest httpRequest) {
    MDC.put("ContextId", httpRequest.getHeader(ORIG_CONTEXT_ID_HEADER));
    MDC.put("SessionId", httpRequest.getHeader(X_CORRELATION_ID));
  }
}
