package sunrise.cps.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestContext {

  /* Sunrise standards */
  private String origRequestId;
  private String origContextId;
  private OriginatorApplication origApp;
  private String origSentTime;
  private String origOperationName;
  private String origLocationId;
  private String origIPAddress;

  private String callerRequestId;
  private CallerApplication callerApp;
  private String callerSentTime;

  private String language;
  private String channel;

  /* SBA specific */
  private String loggedInUser;
  private String loggedInAlias;

  public void clear() {
    this.origIPAddress = null;
    this.origApp = null;
    this.origRequestId = null;
    this.origContextId = null;
    this.origSentTime = null;
    this.origOperationName = null;
    this.origLocationId = null;

    this.callerApp = null;
    this.callerRequestId = null;
    this.callerSentTime = null;

    this.language = null;

    this.loggedInUser = null;
    this.loggedInAlias = null;
    this.channel = null;
  }

}
