package sunrise.cps.common;

public enum Status {
    ACTIVE("ACTIVE"),

    SUSPENDED("SUSPENDED"),

    DELETED("DELETED");

    private String value;

    Status(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static Status fromValue(String value) {
        for (Status b : Status.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
