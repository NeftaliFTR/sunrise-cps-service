package sunrise.cps.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import sunrise.cps.exception.common.ApiErrors;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class CpsApiException extends RuntimeException {
  private ApiErrors apiError;
  private List<String> apiSubErrors;
  private String message;

  public CpsApiException(ApiErrors apiError, String message) {
    super(message);
    this.apiError = apiError;
    this.message = message;
  }

  public CpsApiException(ApiErrors apiError, List<String> apiSubErrors, String message) {
    super(message);
    this.apiError = apiError;
    this.apiSubErrors = apiSubErrors;
    this.message = message;
  }

  public CpsApiException(ApiErrors apiError, Throwable cause) {
    super(cause);
    this.apiError = apiError;
  }

  public CpsApiException(ApiErrors apiError, String message, Throwable cause) {
    super(message, cause);
    this.apiError = apiError;
    this.message = message;
  }
}
