package sunrise.cps.exception.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;
import sunrise.cps.exception.CpsApiException;

import java.util.Collection;
import java.util.List;

//import org.springframework.web.multipart.MultipartFile;
//import sunrise.cps.config.ApplicationConstants;

public class AssertUtil {

    public static void notBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new CpsApiException(ApiErrors.MISSING_PARAMS, message);
        }
    }
    public static void notBlank(String message, String... args) {
        for (String str : args) {
            if (str == null || StringUtils.isBlank(str)) {
                throw new CpsApiException(ApiErrors.MISSING_PARAMS, message);
            }
        }
    }
    public static void lengthGreaterThan(int length, String string, ApiErrors apiError, String message) {
        if (string.length() <= length) {
            throw new CpsApiException(apiError, message);
        }
    }
    public static void sizeGreaterThan(int length, Collection collection, ApiErrors apiError, String message) {
        if (collection.size() <= length) {
            throw new CpsApiException(apiError, message);
        }
    }
    public static void nonNull(@Nullable Object object, String message) {
        if (object == null) {
            throw new CpsApiException(ApiErrors.MISSING_PARAMS, message);
        }
    }
    public static void nonNull(@Nullable Object object, ApiErrors apiError, String message) {
        if (object == null) {
            throw new CpsApiException(apiError, message);
        }
    }
    public static void listNonEmpty(List list, ApiErrors apiError, String message) {
        if (list.isEmpty()) {
            throw new CpsApiException(apiError, message);
        }
    }
    public static void listSize(int size, List list, ApiErrors apiError, String message) {
        if (list.size() !=  size) {
            throw new CpsApiException(apiError, message);
        }
    }
    public static void lengthLessThan(int size, String string, ApiErrors apiError, String message) {
        if (string.length() >= size) {
            throw new CpsApiException(apiError, message);
        }
    }
   /* public static void image( MultipartFile file, ApiErrors apiError, String message) {
        if (file != null) {
            String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
            assert fileExtension != null;
            boolean isImage =
                    ApplicationConstants.ALLOWED_IMAGE_TYPES
                    .stream()
                    .anyMatch(fileExtension::equalsIgnoreCase);
            if(!isImage)
                throw new CPSApiException(apiError, message);
        }
    }
*/
    public static void stringsEqual(String str1, String str2, ApiErrors apiError, String message) {
        if (!str1.equals(str2)) {
            throw new CpsApiException(apiError, message);
        }
    }

    public static void equal(String str1, String str2, ApiErrors apiError, String message) {
        if (!str1.equals(str2)) {
            throw new CpsApiException(apiError, message);
        }
    }


}
