package sunrise.cps.exception.common;


import org.springframework.http.HttpStatus;


/**
 *  Error code semantics : HTTP Status (3 digits) + CPS Code (3 digits)
 */
public enum ApiErrors {

    /******************************** Client Errors *************************/
    BAD_REQUEST(400000, HttpStatus.BAD_REQUEST),
    MISSING_PARAMS(400001, HttpStatus.BAD_REQUEST),
    INVALID_ARGUMENTS(400002, HttpStatus.BAD_REQUEST),
    INVALID_AMOUNT_VALUE(400003, HttpStatus.BAD_REQUEST),
    INVALID_SITE_LIMIT_VALUE(400004, HttpStatus.BAD_REQUEST),
    /******************************** Conflict *************************/
    ACCOUNT_ALREADY_EXISTS(409001, HttpStatus.CONFLICT),
    AVAILABLE_AMOUNT_NOT_ENOUGH_FOR_RESERVE(409002, HttpStatus.CONFLICT),
    SITE_LIMIT_AMOUNT_NOT_ENOUGH_FOR_RESERVE(409003, HttpStatus.CONFLICT),
    /******************************** Forbidden *************************/
    CURRENT_DATE_NOT_MATCH_WITHIN_VALIDITY_START_AND_END_DATE( 403001, HttpStatus.FORBIDDEN),
    /******************************** Unsupported Media type *************************/
    UNSUPPORTED_MEDIA_TYPE(415000, HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    /******************************** Not Found *************************/
    USER_NOT_FOUND(404001, HttpStatus.NOT_FOUND),
    ACCOUNT_NOT_FOUND(404002, HttpStatus.NOT_FOUND),
    BUCKET_NOT_FOUND(404003, HttpStatus.NOT_FOUND),
    /******************************** Server Errors *************************/
    INTERNAL_SERVER_ERROR(500000, HttpStatus.INTERNAL_SERVER_ERROR),
    CONNECTION_ERROR(500001, HttpStatus.INTERNAL_SERVER_ERROR),
    DATA_INTEGRITY_ERROR(500002, HttpStatus.INTERNAL_SERVER_ERROR),
    ENTITY_NOT_FOUND(500003, HttpStatus.NOT_FOUND);

    private final int code;
    private final HttpStatus httpStatus;

    ApiErrors(int code, HttpStatus httpStatus) {
        this.code = code;
        this.httpStatus= httpStatus;
    }


    public int getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return "ApiErrors{" +
                "code=" + code +
                ", httpStatus=" + httpStatus +
                '}';
    }

}
