package sunrise.cps.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import sunrise.cps.exception.common.ApiErrors;

import javax.validation.ConstraintViolationException;
import java.time.OffsetDateTime;
import java.util.Objects;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  /**
   * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter
   * is missing.
   *
   * @param ex      MissingServletRequestParameterException
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return the ApiErrors object
   */
  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse(ex.getMessage(), ex,
        ApiErrors.MISSING_PARAMS, (ServletWebRequest) request);
    return ResponseEntity.status(status).body(responseBody);
  }

  /**
   * Handle HttpMediaTypeNotSupportedException. This one triggers when JSON is invalid as well.
   *
   * @param ex      HttpMediaTypeNotSupportedException
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return the ApiErrors object
   */
  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
      HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getContentType());
    builder.append(" media type is not supported. Supported media types are ");
    ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse(builder.toString(), ex,
        ApiErrors.UNSUPPORTED_MEDIA_TYPE, (ServletWebRequest) request);
    return ResponseEntity.status(status).body(responseBody);
  }

  /**
   * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
   *
   * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return the ApiErrors object
   */
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse("Method Arguments Not Valid Error", ex,
        ApiErrors.INVALID_ARGUMENTS, (ServletWebRequest) request);
    log.error("Field Errors: {}  Global Errors: {}", ex.getBindingResult().getFieldErrors(), ex.getBindingResult().getGlobalErrors());
    return ResponseEntity.status(status).body(responseBody);
  }

  /**
   * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
   *
   * @param ex the ConstraintViolationException
   * @return the ApiErrors object
   */
  @ExceptionHandler(ConstraintViolationException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleConstraintViolation(ConstraintViolationException ex,
      WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse("Validation Error", ex,
        ApiErrors.INVALID_ARGUMENTS, (ServletWebRequest) request);
    return ResponseEntity.status(BAD_REQUEST).body(responseBody);
  }


  /**
   * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
   *
   * @param ex      HttpMessageNotReadableException
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return the ApiErrors object
   */
  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse("Malformed JSON request", ex,
        ApiErrors.BAD_REQUEST, (ServletWebRequest) request);
    return ResponseEntity.status(BAD_REQUEST).body(responseBody);
  }

  /**
   * Handle HttpMessageNotWritableException.
   *
   * @param ex      HttpMessageNotWritableException
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return the ApiErrors object
   */
  @Override
  protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse("Error writing JSON output", ex,
        ApiErrors.INTERNAL_SERVER_ERROR, (ServletWebRequest) request);
    return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(responseBody);
  }

  /**
   * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
   *
   * @param ex the DataIntegrityViolationException
   * @return the ApiErrors object
   */
  @ExceptionHandler(DataIntegrityViolationException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleDataIntegrityViolation(DataIntegrityViolationException ex,
      WebRequest request) {
    if (ex.getCause() instanceof ConstraintViolationException) {
      return buildResponseEntity("Database error", ex, HttpStatus.CONFLICT,
          ApiErrors.DATA_INTEGRITY_ERROR, request);
    }
    return buildResponseEntity("Unexpected error", ex, INTERNAL_SERVER_ERROR,
        ApiErrors.INTERNAL_SERVER_ERROR, request);
  }

  @ExceptionHandler(IncorrectResultSizeDataAccessException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleIncorrectResultSizeException(
      IncorrectResultSizeDataAccessException ex, WebRequest request) {
    return buildResponseEntity("Data error", ex, INTERNAL_SERVER_ERROR,
        ApiErrors.INTERNAL_SERVER_ERROR, request);
  }

  /**
   * Handle Exception, handle generic Exception.class
   *
   * @param ex the Exception
   * @return the ApiErrors object
   */
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException ex, WebRequest request) {
    String message = String
        .format("The parameter '%s' of value '%s' could not be converted to type '%s'",
            ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());
    return buildResponseEntity("Missing Arguments", ex, BAD_REQUEST,
        ApiErrors.BAD_REQUEST, request);
  }

  @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleEntityNotFound(
      javax.persistence.EntityNotFoundException ex, WebRequest request) {
    return buildResponseEntity("Entity Not Found", ex, HttpStatus.NOT_FOUND,
        ApiErrors.ENTITY_NOT_FOUND, request);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<sunrise.cps.api.model.CpsApiException> handleBadRequest(final IllegalArgumentException e,
      WebRequest request) {
    return buildResponseEntity(e.getMessage(), e, BAD_REQUEST, ApiErrors.BAD_REQUEST,
        request);
  }

  @ExceptionHandler(HttpClientErrorException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleHttpClientError(
      HttpClientErrorException ex, WebRequest request) {
    return buildResponseEntity(ex.getMessage(), ex, ex.getStatusCode(),
        ApiErrors.CONNECTION_ERROR, request);
  }

  @ExceptionHandler(RestClientException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleRestClientError(
      HttpClientErrorException ex, WebRequest request) {
    return buildResponseEntity(ex.getMessage(), ex, ex.getStatusCode(),
        ApiErrors.INTERNAL_SERVER_ERROR, request);
  }

  @ExceptionHandler(sunrise.cps.exception.CpsApiException.class)
  protected ResponseEntity<sunrise.cps.api.model.CpsApiException> handleApiException(
          sunrise.cps.exception.CpsApiException ex, WebRequest request) {
    log.error("CpsApiException :: ", ex);
    return buildResponseEntity(ex.getMessage(), ex, ex.getApiError(), request);
  }

  private ResponseEntity<sunrise.cps.api.model.CpsApiException> buildResponseEntity(String message, CpsApiException ex,
      ApiErrors apiError, WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse(message, ex,
        apiError, (ServletWebRequest) request);
    return ResponseEntity.status(responseBody.getStatus()).body(responseBody);
  }

  private ResponseEntity<sunrise.cps.api.model.CpsApiException> buildResponseEntity(
      String message, Exception ex, HttpStatus status, ApiErrors error, WebRequest request) {
    sunrise.cps.api.model.CpsApiException responseBody = createErrorResponse(message, ex,
        error, (ServletWebRequest) request);
    return ResponseEntity.status(status).body(responseBody);
  }

  private sunrise.cps.api.model.CpsApiException createErrorResponse(String message,
      Exception ex,
      ApiErrors error, ServletWebRequest request) {
    sunrise.cps.api.model.CpsApiException apiException = new sunrise.cps.api.model.CpsApiException();
    return apiException
        .timestamp(OffsetDateTime.now())
        .exception(ex.getClass().getName())
        .path(request.getRequest().getServletPath())
        .message(message)
        .debugMessage(ex.getLocalizedMessage())
        .status(error.getHttpStatus().value())
        .code(error.getCode())
        .name(mapErrorName(error));
  }

  private sunrise.cps.api.model.ErrorName mapErrorName(ApiErrors error) {
    if(Objects.isNull(error)) {
      return sunrise.cps.api.model.ErrorName.INTERNAL_SERVER_ERROR;
    }
    return sunrise.cps.api.model.ErrorName.valueOf(error.name());
  }

}
