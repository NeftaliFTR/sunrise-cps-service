package sunrise.cps.exception.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Configuration("AuthExceptionHandlingConfiguration")
@Slf4j
public class ExceptionHandlingConfiguration {

    // TODO later add Clarify exception processor here

}
