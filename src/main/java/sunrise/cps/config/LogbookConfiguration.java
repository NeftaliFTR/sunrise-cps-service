package sunrise.cps.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.zalando.logbook.BodyFilter;
import org.zalando.logbook.BodyFilters;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class LogbookConfiguration {

  @Bean
  @Profile("prod")
  public BodyFilter bodyFilter() {
    final Set<String> properties = new HashSet<>();
    properties.add("access_token");
    properties.add("open_id");
    properties.add("id_token");
    properties.add("image");
    properties.add("password");
    return BodyFilters.replaceJsonStringProperty(properties, "*****");
  }

  @Bean
  @Profile("!prod")
  public BodyFilter bodyFilterNotProd() {
    final Set<String> properties = new HashSet<>();
    properties.add("image");
    return BodyFilters.replaceJsonStringProperty(properties, "*****");
  }

}
