package sunrise.cps.config;

import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.target.ThreadLocalTargetSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.*;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import sunrise.cps.common.RequestContext;

import javax.servlet.Filter;

@Configuration
@Import({BeanValidatorPluginsConfiguration.class})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {

    @Autowired
    @Qualifier("headers-filter")
    public Filter headersFilter;

  @SuppressWarnings("unchecked")
    @Bean
    public FilterRegistrationBean headersFilterRegistration() {
        FilterRegistrationBean result = new FilterRegistrationBean();
        result.setFilter(headersFilter);
        result.addUrlPatterns("/*");
        result.setName("HeadersFilter");
        result.setOrder(1);
        return result;
    }

    @Bean(destroyMethod = "destroy")
    public ThreadLocalTargetSource threadLocalRequestContext() {
        ThreadLocalTargetSource result = new ThreadLocalTargetSource();
        result.setTargetBeanName("requestContext");
        return result;
    }

    @Primary
    @Bean(name = "proxiedThreadLocalTargetSource")
    public ProxyFactoryBean proxiedThreadLocalTargetSource(ThreadLocalTargetSource threadLocalTargetSource) {
        ProxyFactoryBean result = new ProxyFactoryBean();
        result.setTargetSource(threadLocalTargetSource);
        return result;
    }

    @Bean(name = "requestContext")
    @Scope(scopeName = "prototype")
    public RequestContext requestContext() {
        return new RequestContext();
    }
}
