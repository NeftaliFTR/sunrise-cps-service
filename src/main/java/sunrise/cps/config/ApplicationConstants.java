package sunrise.cps.config;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ApplicationConstants {

  public static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";
  public static final String SPRING_PROFILE_DEVELOPMENT = "h2";
  public static final ZoneId DEFAULT_TIME_ZONE = ZoneId.of("Europe/Paris");
  public static final List<String> ALLOWED_IMAGE_TYPES = Collections
      .unmodifiableList(Arrays.asList("jpg", "jpeg", "png", "bmp"));
}
