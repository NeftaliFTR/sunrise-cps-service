package sunrise.cps.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@Slf4j
@EnableTransactionManagement
@EnableJpaRepositories(
    transactionManagerRef = "cpsTransactionManager",
    entityManagerFactoryRef = "cpsEntityManagerFactory",
    basePackages = {"sunrise.cps.domain.entities"}
)
public class CpsAuthDBConfig {

    @Value("${spring.datasource.hikari.maximum-pool-size}")
    private Integer maxPoolSize;


    @Value("${spring.datasource.hikari.minimum-idle}")
    private Integer minIdle;

    @Value("${spring.datasource.url}")
    private String jdbcUrl;

    @Value("${spring.datasource.hikari.idle-timeout}")
    private Integer idleTimeout;

    @Value("${spring.datasource.hikari.pool-name}")
    private String poolName;

    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Primary
    @Bean(name = "cpsDataSource")
    public HikariDataSource dataSource() {
        final HikariDataSource ds = new HikariDataSource();
        ds.setMaximumPoolSize(maxPoolSize);
        ds.setMinimumIdle(minIdle);
        ds.setDriverClassName(driverClassName);
        ds.setJdbcUrl(jdbcUrl);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setIdleTimeout(idleTimeout);
        ds.setPoolName(poolName);
        return ds;
    }



    @Primary
    @Bean(name = "cpsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("cpsDataSource") HikariDataSource dataSource) {
        log.info("Creating CPS datasource {}", dataSource);
        return builder
                .dataSource(dataSource)
                .packages("sunrise.cps.domain.entities")
                .persistenceUnit("sunrise.cps")
                .build();
    }

    @Primary
    @Bean(name = "cpsTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("cpsEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
