package sunrise.cps.api.core.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sunrise.cps.api.BaseController;
import sunrise.cps.api.SubsidyApi;
import sunrise.cps.api.model.*;
import sunrise.cps.service.core.SubsidyService;
import sunrise.cps.service.core.SubsidyService;

import javax.validation.Valid;

@Slf4j
@RestController
public class SubsidyApiImpl extends BaseController implements SubsidyApi {
    @Autowired
    private SubsidyService subsidyService;


    @Override
    public ResponseEntity<BucketDetailsResponse> getBucketDetails(@PathVariable("accountNumber") String accountNumber) {
        log.info("getBucketDetails called for accountNumber {}", accountNumber);
        return ResponseEntity.ok(new BucketDetailsResponse().subsidy(subsidyService.getBucketDetails(accountNumber)));

    }

    @Override
    public ResponseEntity<GenericIdModel>createBucketDetails(@PathVariable("accountNumber")
        String accountNumber, @RequestBody CreateBucketRequest createBucketRequest){
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

   @Override
    public ResponseEntity<BalanceDetailsResponse>getBalanceDetails(String accountNumber,
       String siteId, String balanceType) {
       log.info("getBalanceDetails called for accountNumber {}, siteId {}, balanceType {}", accountNumber, siteId,balanceType);
       return ResponseEntity.ok(subsidyService.getBalanceDetails(accountNumber,siteId,balanceType));
    }
}
