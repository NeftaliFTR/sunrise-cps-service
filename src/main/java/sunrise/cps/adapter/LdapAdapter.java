package sunrise.cps.adapter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Component("sba.ldap.adapter")
@Slf4j
public class LdapAdapter {

    @Value("${application.ntuser.active.directory.url}")
    private String ldapUrl;

    @Value("${application.ntuser.active.directory.managerDn}")
    private String managerDn;

    @Value("${application.ntuser.active.directory.managerPwd}")
    private String managerPwd;

    @Value("${application.ldapadapter.ldap.baseDN}")
    private String baseDn;

    public boolean authorizeUser(String username, String password) {
        try {
            SearchResult searchResult = searchAgent(username);
            if (null != searchResult) {
                log.debug("Found agent [{}], try authentication ", username);
                return tryAuthentication(searchResult.getNameInNamespace(), password);
            }
        } catch (NamingException e) {
            log.debug("Failed to authorize agent [{}] with reason {}] ", username, e.getMessage(), e);
        }
        log.debug("Agent login [{} doesn't exist in LDAP ", username);
        return false;
    }
    
    public List<SearchResult> getAllUsers(String username, String firstname, String lastname) {
        List<SearchResult> searchResult = new ArrayList<>();

        StringBuilder filterStr = new StringBuilder("(&");

        if(StringUtils.isNotBlank(username)) {
            filterStr.append("(sAMAccountName=*").append(username).append("*)");
        }
        if(StringUtils.isNotBlank(firstname)) {
            filterStr.append("(givenName=*").append(firstname).append("*)");
        }
        if(StringUtils.isNotBlank(lastname)) {
            filterStr.append("(sn=*").append(lastname).append("*)");
        }
        filterStr.append(")");

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setDerefLinkFlag(true);
        searchControls.setTimeLimit(5000);

        try {
            DirContext ctx = getMasterLdapContext();
            log.debug("Master LDAP login is ok");
            NamingEnumeration<SearchResult> results = ctx.search(baseDn, filterStr.toString(), searchControls);
            while (results.hasMoreElements()) {
                searchResult.add(results.nextElement());
            }
        } catch (NamingException e) {
            log.debug("Failed to getAllUsers for agent [{}], firstname [{}], lastname [{}] with reason {}] ", username, firstname, lastname, e.getMessage(), e);
        }
        return searchResult;
    }

    public SearchResult searchAgentForUsername(String username) throws NamingException {
        return searchAgent(username);
    }

    private boolean tryAuthentication(String principal, String credentials) throws NamingException {
        try {
            initLdapContext(principal, credentials);
            return true;
        } catch (javax.naming.AuthenticationException e) {
            log.debug("Failed to authenticate agent [{}] against LDAP with error [{}] ", principal, e.getMessage(), e);
            return false;
        }
    }

    private InitialDirContext initLdapContext(String principal, String credentials) throws NamingException {
        log.debug("Creating LDAP context for principal [{}] ", principal);
        InitialDirContext customerLdapContext;
        Hashtable<String, Object> env = new Hashtable<>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, principal);
        env.put(Context.SECURITY_CREDENTIALS, credentials);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapUrl);
        customerLdapContext = new InitialDirContext(env);
        log.debug("Successfully created LDAP context for principal [{}] ", principal);
        return customerLdapContext;
    }

    private SearchResult searchAgent(String username) throws NamingException {

        log.debug("Searching for agent [{}] in LDAP...", username);
        String searchFilter = "(sAMAccountName=" + username + ")";
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setDerefLinkFlag(true);
        searchControls.setTimeLimit(5000);

        DirContext ctx = getMasterLdapContext();
        log.debug("Master LDAP login is ok");
        NamingEnumeration<SearchResult> results = ctx.search(baseDn, searchFilter, searchControls);
        log.debug("Finished search for agent [{}].", username);

        SearchResult searchResult = null;
        if (results.hasMoreElements()) {
            searchResult = results.nextElement();

            // make sure there is not another item available, there should be
            // only 1 match
            if (results.hasMoreElements()) {
                log.error("Matched multiple users for the accountName: [{}]", username);
            }
        }

        return searchResult;
    }

    private InitialDirContext getMasterLdapContext() throws NamingException {
        return initLdapContext(managerDn, managerPwd);
    }

}
