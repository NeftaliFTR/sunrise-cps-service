BEGIN
  IF NOT SCHEMA_SUPPORT.is_table_exists('CPS_API_USERS') THEN
    EXECUTE IMMEDIATE 'CREATE TABLE CPS_API_USERS  
( 
ID NUMBER(38,0) NOT NULL ,    
USERNAME VARCHAR2(20 BYTE) NOT NULL ,    
CONSTRAINT CPS_API_USERS_PK PRIMARY KEY (ID)     
)TABLESPACE CPS_DATA';
  END IF;
  
  IF NOT SCHEMA_SUPPORT.is_table_exists('CPS_ACCOUNTS') THEN
    EXECUTE IMMEDIATE 'CREATE TABLE CPS_ACCOUNTS  
( 
ID NUMBER(38,0) NOT NULL , 
CREATION_TIME TIMESTAMP (6) DEFAULT CURRENT_TIMESTAMP NOT NULL , 
CHANGE_TIME TIMESTAMP (6) ,    
ACCOUNT_NUMBER VARCHAR2(255 BYTE) NOT NULL ,    
ACCOUNT_NAME   VARCHAR2(255 BYTE) NOT NULL ,    
STATUS VARCHAR2(255 BYTE) NOT NULL , 
MAIN_SITE_ID VARCHAR2(255 BYTE) NOT NULL ,    
CONSTRAINT CPS_ACCOUNTS_PK PRIMARY KEY (ID)  
)TABLESPACE CPS_DATA';
  END IF;
  
  IF NOT SCHEMA_SUPPORT.is_table_exists('CPS_SUBSIDY_BUCKET') THEN
    EXECUTE IMMEDIATE 'CREATE TABLE CPS_SUBSIDY_BUCKET  
(    
ID            NUMBER(38,0) NOT NULL , 
CREATION_TIME TIMESTAMP (6) DEFAULT CURRENT_TIMESTAMP NOT NULL ,    
CHANGE_TIME TIMESTAMP (6) ,    
REF_ID VARCHAR2(255 BYTE) NOT NULL,     
TOTAL_AMOUNT  NUMBER(38,2) NOT NULL , 
USED_AMOUNT   NUMBER(38,2) NOT NULL ,    
VALIDITY_START_DATE DATE NOT NULL ,    
VALIDITY_END_DATE DATE NOT NULL ,    
STATUS     VARCHAR2(255 BYTE) NOT NULL ,    
ACCOUNT_ID NUMBER(38,0) NOT NULL ,    
CONSTRAINT CPS_SUBSIDY_BUCKET_PK PRIMARY KEY (ID),    
CONSTRAINT CPS_SUBSIDY_BUCKET_REF_ID_UK UNIQUE (REF_ID),    
CONSTRAINT CPS_SUBSIDY_BUCKET_ACCOUNT_ID_FK FOREIGN KEY (ACCOUNT_ID) REFERENCES CPS_ACCOUNTS (ID)  
)TABLESPACE CPS_DATA';
  END IF;
  
  IF NOT SCHEMA_SUPPORT.is_table_exists('CPS_SITES_LIMIT') THEN
    EXECUTE IMMEDIATE 'CREATE TABLE CPS_SITES_LIMIT  
(    
ID            NUMBER(38,0) NOT NULL , 
CREATION_TIME TIMESTAMP (6) DEFAULT CURRENT_TIMESTAMP NOT NULL ,    
CHANGE_TIME TIMESTAMP (6) ,    
SITE_ID       VARCHAR2(255 BYTE) NOT NULL ,    
BUCKET_REF_ID VARCHAR2(255 BYTE) NOT NULL ,    
AMOUNT_LIMIT  NUMBER(38,2) ,    
CONSTRAINT CPS_SITES_LIMIT_PK PRIMARY KEY (ID),    
CONSTRAINT CPS_SITES_LIMIT_BUCKET_REF_ID_FK FOREIGN KEY (BUCKET_REF_ID) REFERENCES CPS_SUBSIDY_BUCKET (REF_ID)  
)TABLESPACE CPS_DATA';
  END IF;
  
  IF NOT SCHEMA_SUPPORT.is_table_exists('CPS_DATA_PROCESS') THEN
    EXECUTE IMMEDIATE 'CREATE TABLE CPS_DATA_PROCESS  
(    
ID            NUMBER(38,0) NOT NULL , 
CREATION_TIME TIMESTAMP (6) DEFAULT CURRENT_TIMESTAMP NOT NULL ,    
CHANGE_TIME TIMESTAMP (6) ,    
PROCESS_ID    VARCHAR2(255 BYTE) NOT NULL ,    
BUCKET_REF_ID VARCHAR2(255 BYTE) NOT NULL ,    
SITE_ID       VARCHAR2(255 BYTE) NOT NULL ,    
STATUS        VARCHAR2(255 BYTE) NOT NULL ,    
AMOUNT        NUMBER(38,2) NOT NULL ,    
CONSTRAINT CPS_DATA_PROCESS_PK PRIMARY KEY (ID),    
CONSTRAINT CPS_DATA_PROCESS_PROCESS_ID_UK UNIQUE (PROCESS_ID),    
CONSTRAINT CPS_DATA_PROCESS_BUCKET_REF_ID_FK FOREIGN KEY (BUCKET_REF_ID) REFERENCES CPS_SUBSIDY_BUCKET (REF_ID)  
)TABLESPACE CPS_DATA';
  END IF;
  
  IF schema_support.is_table_exists('CPS_API_USERS') THEN
    IF NOT schema_support.is_index_exists('CPS_API_USERNAME_IDX') THEN
      EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX CPS_API_USERNAME_IDX ON CPS_API_USERS(USERNAME) TABLESPACE CPS_INDX';
    END IF;
  END IF;
  IF schema_support.is_table_exists('CPS_ACCOUNTS') THEN
    IF NOT schema_support.is_index_exists('CPS_ACCOUNTS_ACCOUNT_NUMBER_IDX') THEN
      EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX CPS_ACCOUNTS_ACCOUNT_NUMBER_IDX ON CPS_ACCOUNTS(ACCOUNT_NUMBER) TABLESPACE CPS_INDX';
    END IF;
  END IF;
  IF schema_support.is_table_exists('CPS_SUBSIDY_BUCKET') THEN
    IF NOT schema_support.is_index_exists('CPS_SUBSIDY_BUCKET_ACCOUNT_ID_IDX') THEN
      EXECUTE IMMEDIATE 'CREATE INDEX CPS_SUBSIDY_BUCKET_ACCOUNT_ID_IDX ON CPS_SUBSIDY_BUCKET(ACCOUNT_ID) TABLESPACE CPS_INDX';
    END IF;
  END IF;
  IF schema_support.is_table_exists('CPS_SITES_LIMIT') THEN
    IF NOT schema_support.is_index_exists('CPS_SITES_LIMIT_SITE_ID_IDX') THEN
      EXECUTE IMMEDIATE 'CREATE INDEX CPS_SITES_LIMIT_SITE_ID_IDX ON CPS_SITES_LIMIT(SITE_ID) TABLESPACE CPS_INDX';
    END IF;
    IF NOT schema_support.is_index_exists('CPS_SITES_LIMIT_BUCKET_REF_ID_IDX') THEN
      EXECUTE IMMEDIATE 'CREATE INDEX CPS_SITES_LIMIT_BUCKET_REF_ID_IDX ON CPS_SITES_LIMIT(BUCKET_REF_ID) TABLESPACE CPS_INDX';
    END IF;
  END IF;
  IF schema_support.is_table_exists('CPS_DATA_PROCESS') THEN
    IF NOT schema_support.is_index_exists('CPS_DATA_PROCESS_BUCKET_REF_ID_IDX') THEN
      EXECUTE IMMEDIATE 'CREATE INDEX CPS_DATA_PROCESS_BUCKET_REF_ID_IDX ON CPS_DATA_PROCESS(BUCKET_REF_ID) TABLESPACE CPS_INDX';
    END IF;
  END IF;

  -- sequence
  IF NOT schema_support.is_sequence_exists('HIBERNATE_SEQUENCE') THEN
    EXECUTE IMMEDIATE 'CREATE SEQUENCE HIBERNATE_SEQUENCE
  MINVALUE 1000
  MAXVALUE 999999999999999999999999999
  START WITH     1000
  INCREMENT BY   1
  CACHE 20
  ';
  END IF;
END;
/
COMMIT;
EXIT;