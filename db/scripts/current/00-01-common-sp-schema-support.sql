CREATE OR REPLACE PACKAGE schema_support authid current_user AS
  /*
   * authid current_user 
   *   means that the rights of the user executing the procedure are applied
   *   instead of the rights of the user which created the procedure
   * in essance this means that the command create_table and drop_table can also be used
   */ 
  C_APP_NAME CONSTANT VARCHAR2(255) := 'CPS';
  C_INDEX_TABLESPACE CONSTANT VARCHAR2(255) := 'CPS_INDX';
  C_GLOBAL_NAME_DEV CONSTANT VARCHAR(255) := 'DCPS01.SWI.SRSE.NET';
  C_GLOBAL_NAME_TEST1 CONSTANT VARCHAR(255) := 'TCPS01.SWI.SRSE.NET';
  C_GLOBAL_NAME_TEST2 CONSTANT VARCHAR(255) := 'TCPS02.SWI.SRSE.NET';
  C_GLOBAL_NAME_PROD CONSTANT VARCHAR(255) := 'PCPS01.SWI.SRSE.NET';

  FUNCTION is_table_exists(p_tablename IN VARCHAR2) RETURN boolean;
  FUNCTION is_create_table(p_tablename IN VARCHAR2) RETURN boolean;
  FUNCTION is_drop_table(p_tablename IN VARCHAR2) RETURN boolean;
  FUNCTION is_column_exists(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean;
  FUNCTION is_add_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean;
  FUNCTION is_add_fk(p_tablename IN VARCHAR2, p_fkname IN VARCHAR2) RETURN boolean;
  FUNCTION is_pk_exists(p_tablename IN VARCHAR2) RETURN boolean;
  FUNCTION is_constraint_exists(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2) RETURN boolean;
  FUNCTION is_drop_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2) RETURN boolean;
  FUNCTION is_create_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2) RETURN boolean;
  FUNCTION is_column_null(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean;
  FUNCTION is_column_notnull(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean;
  FUNCTION is_index_exists(p_indexname IN VARCHAR2) RETURN boolean;
  FUNCTION is_create_index(p_indexname IN VARCHAR2) RETURN boolean;
  FUNCTION is_drop_index(p_indexname IN VARCHAR2) RETURN boolean;
  FUNCTION is_tablespace_exist(p_tablespace IN VARCHAR2) RETURN boolean;
  FUNCTION is_procedure_exists(p_procedurename IN VARCHAR2) RETURN boolean;
  FUNCTION is_sequence_exists(p_sequencename IN VARCHAR2) RETURN boolean;
  FUNCTION is_user_exists(p_username IN VARCHAR2) RETURN boolean;
  FUNCTION get_tablespace_command(p_tablespace IN VARCHAR2) RETURN VARCHAR2;
  
  FUNCTION is_env   (p_envname IN VARCHAR2) RETURN boolean;
  FUNCTION is_prod  RETURN boolean;
  FUNCTION is_test1 RETURN boolean;
  FUNCTION is_test2 RETURN boolean;
  FUNCTION is_dev   RETURN boolean;
  
  PROCEDURE execute_if_zero(p_counter IN NUMBER, p_command IN VARCHAR2);
  PROCEDURE execute_if_true(p_flag IN BOOLEAN, p_command IN VARCHAR2);
  PROCEDURE execute_if_prod  (p_command IN VARCHAR2);
  PROCEDURE execute_if_test1 (p_command IN VARCHAR2);
  PROCEDURE execute_if_test2 (p_command IN VARCHAR2);
  PROCEDURE execute_if_dev   (p_command IN VARCHAR2);
  PROCEDURE execute_command(p_command IN VARCHAR2);
  
  PROCEDURE log_rows_updated(p_tablename IN VARCHAR2, p_command IN VARCHAR2 := 'updated', p_prefix IN VARCHAR2 := 'post');
  PROCEDURE log_with_date(p_message IN VARCHAR2);

  PROCEDURE create_table(p_tablename IN VARCHAR2, p_createtablecommand IN VARCHAR2);
  PROCEDURE drop_table(p_tablename IN VARCHAR2, p_cascade_constraints IN BOOLEAN := true);
  PROCEDURE rename_table(p_oldtablename IN VARCHAR2, p_newtablename IN VARCHAR2);
  PROCEDURE add_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2, p_columntype IN VARCHAR2);
  PROCEDURE drop_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2);
  PROCEDURE force_drop_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2);
  PROCEDURE add_fk(p_tablename IN VARCHAR2, p_fkname IN VARCHAR2, p_columnname IN VARCHAR2, p_referencetablename IN VARCHAR2);
  PROCEDURE create_pk(p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2);
  PROCEDURE drop_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2);
  PROCEDURE create_sequence(p_sequencename IN VARCHAR2);
  PROCEDURE drop_sequence(p_sequencename IN VARCHAR2);
  PROCEDURE create_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2, p_constraintSql IN VARCHAR2);
  PROCEDURE rename_constraint(p_tablename IN VARCHAR2, p_oldname IN VARCHAR2, p_newname IN VARCHAR2);
  
  PROCEDURE set_column_notnull(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2);
  PROCEDURE set_column_null(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2);
  PROCEDURE rename_column(p_tablename IN VARCHAR2, p_from_columnname IN VARCHAR2, p_to_columnname IN VARCHAR2);  

  PROCEDURE create_index(p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE);
  PROCEDURE create_bitmap_index(p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE);
  PROCEDURE create_unique_index(p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE);
  PROCEDURE create_index_of_type(p_indextype IN VARCHAR2, p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE);
  PROCEDURE drop_index(p_indexname IN VARCHAR2);
  PROCEDURE drop_procedure(p_procedurename IN VARCHAR2);
  PROCEDURE grant_table_access(p_tablename IN VARCHAR2, p_username IN VARCHAR2, p_select IN BOOLEAN := TRUE, p_insert IN BOOLEAN := TRUE, p_update IN BOOLEAN := TRUE, p_delete IN BOOLEAN := TRUE);
  PROCEDURE grant_command(p_objectname IN VARCHAR2, p_command IN VARCHAR2, p_username IN VARCHAR2);
  PROCEDURE grant_execute(p_objectname IN VARCHAR2, p_username IN VARCHAR2);

END schema_support;
/


CREATE OR REPLACE PACKAGE BODY SCHEMA_SUPPORT AS

  FUNCTION is_table_exists(p_tablename IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    SELECT COUNT(*) INTO m_counter 
    FROM USER_TABLES
    WHERE UPPER(TABLE_NAME) = UPPER(p_tablename);
    return m_counter > 0;
  END is_table_exists;

  FUNCTION is_create_table(p_tablename IN VARCHAR2) RETURN boolean
  IS
  BEGIN
    return not(is_table_exists(p_tablename));
  END is_create_table;
  
  FUNCTION is_drop_table(p_tablename IN VARCHAR2) RETURN boolean
  IS
  BEGIN
    return is_table_exists(p_tablename);
  END is_drop_table;

  FUNCTION is_column_exists(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    SELECT COUNT(*) INTO m_counter 
    FROM USER_TAB_COLUMNS 
    WHERE 
      UPPER(TABLE_NAME) = UPPER(p_tablename) 
    AND 
      UPPER(COLUMN_NAME) = UPPER(p_columnname);
    return m_counter > 0;
  END is_column_exists;

  FUNCTION is_add_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
      return not(is_column_exists(p_tablename, p_columnname));
  END is_add_column;

  FUNCTION is_drop_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
      return is_column_exists(p_tablename, p_columnname);
  END is_drop_column;

  FUNCTION is_add_fk(p_tablename IN VARCHAR2, p_fkname IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    SELECT COUNT(*) INTO m_counter 
    FROM USER_CONSTRAINTS
    WHERE UPPER(TABLE_NAME) = UPPER(p_tablename)
	AND UPPER(CONSTRAINT_NAME) = upper(p_fkname);
    
    return m_counter = 0;
  END is_add_fk;
  
  FUNCTION is_pk_exists(p_tablename IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    SELECT COUNT(*) INTO m_counter 
    FROM USER_CONSTRAINTS
    WHERE UPPER(TABLE_NAME) = UPPER(p_tablename)
	AND CONSTRAINT_TYPE = 'P';
    return m_counter != 0;
  END is_pk_exists;
  
  FUNCTION is_constraint_exists(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    SELECT COUNT(*) INTO m_counter 
    FROM USER_CONSTRAINTS
    WHERE UPPER(TABLE_NAME) = UPPER(p_tablename)
	AND UPPER(CONSTRAINT_NAME) = upper(p_constraintname);
    
    return m_counter != 0;
  END is_constraint_exists;  

  FUNCTION is_drop_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
    return is_constraint_exists(p_tablename, p_constraintname);
  END is_drop_constraint;  
  
  FUNCTION is_create_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
    return not(is_constraint_exists(p_tablename, p_constraintname));
  END is_create_constraint;

  FUNCTION is_column_null(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
    return not(is_column_notnull(p_tablename, p_columnname));
  END is_column_null;
 
  FUNCTION is_column_notnull(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    select count(*) into m_counter 
    from USER_CONS_COLUMNS acc, USER_CONSTRAINTS ac
    where acc.CONSTRAINT_NAME = ac.CONSTRAINT_NAME
    and UPPER(acc.TABLE_NAME) = UPPER(p_tablename)
    and UPPER(acc.COLUMN_NAME) = UPPER(p_columnname)
    and ac.CONSTRAINT_TYPE = 'C';
    return m_counter != 0;
  END is_column_notnull;
  
  FUNCTION is_index_exists(p_indexname IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    select count(*) into m_counter 
    from USER_INDEXES 
    where INDEX_NAME = UPPER(p_indexname);
    return m_counter != 0;
  END is_index_exists;
  FUNCTION is_create_index(p_indexname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
  	return not(is_index_exists(p_indexname));
  END is_create_index;
  FUNCTION is_drop_index(p_indexname IN VARCHAR2) RETURN boolean
  IS
  BEGIN
  	return is_index_exists(p_indexname);
  END is_drop_index;

  FUNCTION is_tablespace_exist(p_tablespace IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
    select count(*) into m_counter 
    from USER_TABLESPACES
    where TABLESPACE_NAME = UPPER(p_tablespace);
    return m_counter != 0;
  END is_tablespace_exist;
  
  FUNCTION is_procedure_exists(p_procedurename IN VARCHAR2) RETURN boolean 
  IS
    m_counter NUMBER;
  BEGIN
    select count(*) into m_counter 
    from USER_PROCEDURES
    where OBJECT_NAME = UPPER(p_procedurename)
    and PROCEDURE_NAME is null;
    return m_counter != 0;
  END is_procedure_exists;
  
  FUNCTION is_sequence_exists(p_sequencename IN VARCHAR2) RETURN boolean 
  IS
    m_counter NUMBER;
  BEGIN
  	SELECT COUNT(*) INTO m_counter 
  	FROM USER_SEQUENCES 
  	WHERE UPPER(USER_SEQUENCES.SEQUENCE_NAME) = UPPER(p_sequencename);
    return m_counter != 0;
  END is_sequence_exists;
  
  FUNCTION is_user_exists(p_username IN VARCHAR2) RETURN boolean
  IS
    m_counter NUMBER;
  BEGIN
  	SELECT COUNT(*) INTO m_counter 
  	FROM ALL_USERS 
  	WHERE UPPER(USERNAME) = UPPER(p_username);
    return m_counter != 0;
  END is_user_exists;
  
  FUNCTION get_tablespace_command(p_tablespace IN VARCHAR2) RETURN VARCHAR2 
  IS
  BEGIN
  	IF is_tablespace_exist(p_tablespace) THEN
  	  return ' tablespace '|| p_tablespace ||' ';
  	END IF;
  	-- default to not specify a tablespace
  	return ' ';
  END get_tablespace_command;
  
  FUNCTION is_env   (p_envname IN VARCHAR2) RETURN boolean
  IS
  	m_envname VARCHAR2(255);
  BEGIN
	  select SYS_CONTEXT ('USERENV', 'DB_NAME') into m_envname FROM DUAL;
	  return m_envname = p_envname;
  END is_env;
  
  FUNCTION is_prod RETURN boolean
  IS
  BEGIN
  	return is_env(C_GLOBAL_NAME_PROD);
  END is_prod;
  
  FUNCTION is_test1 RETURN boolean
  IS
  BEGIN
  	return is_env(C_GLOBAL_NAME_TEST1);
  END is_test1; 
  
  FUNCTION is_test2 RETURN boolean
  IS
  BEGIN
  	return is_env(C_GLOBAL_NAME_TEST2);
  END is_test2; 
  
  FUNCTION is_dev   RETURN boolean
  IS
  BEGIN
  	return is_env(C_GLOBAL_NAME_DEV);
  END is_dev; 
  
  PROCEDURE execute_if_zero(p_counter IN NUMBER, p_command IN VARCHAR2)
  IS
    m_flag BOOLEAN;
  BEGIN
    m_flag := p_counter = 0;
	execute_if_true(m_flag, p_command);
  END execute_if_zero;
  
  PROCEDURE execute_if_prod  (p_command IN VARCHAR2)
  IS
  BEGIN
	  execute_if_true(is_prod(), p_command);
  END execute_if_prod;
  PROCEDURE execute_if_test1 (p_command IN VARCHAR2)
  IS
  BEGIN
	  execute_if_true(is_test1(), p_command);
  END execute_if_test1;
  PROCEDURE execute_if_test2 (p_command IN VARCHAR2)
  IS
  BEGIN
	  execute_if_true(is_test2(), p_command);
  END execute_if_test2;
  PROCEDURE execute_if_dev   (p_command IN VARCHAR2)
  IS
  BEGIN
	  execute_if_true(is_dev(), p_command);
  END execute_if_dev;
  
  PROCEDURE execute_if_true(p_flag IN BOOLEAN, p_command IN VARCHAR2)
  IS
  BEGIN
    IF p_flag THEN
		execute_command(p_command);
	END IF;
  END execute_if_true;

  PROCEDURE execute_command(p_command IN VARCHAR2)
  IS
  BEGIN
    log_with_date('pre  - ' || p_command);
    EXECUTE IMMEDIATE p_command;
    log_with_date('post  - ' || p_command);
  END execute_command;

  PROCEDURE log_rows_updated(p_tablename IN VARCHAR2, p_command IN VARCHAR2 := 'updated', p_prefix IN VARCHAR2 := 'post')
  IS
  BEGIN
    log_with_date(p_prefix || ' - '|| sql%rowcount || ' rows ' || p_command || ' for table ' || p_tablename);
  END log_rows_updated;
  
  PROCEDURE log_with_date(p_message IN VARCHAR2)
  IS
  	msg VARCHAR2(240);
  BEGIN
    select substr (TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS ') || p_message, 1, 240) into msg from dual;
    DBMS_OUTPUT.PUT_LINE(msg);
  END log_with_date;

  PROCEDURE create_table(p_tablename IN VARCHAR2, p_createtablecommand IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_create_table(p_tablename), p_createtablecommand);
  END create_table;
  
  PROCEDURE drop_table(p_tablename IN VARCHAR2, p_cascade_constraints IN BOOLEAN := true)
  IS
  BEGIN
	IF p_cascade_constraints THEN
	  execute_if_true(is_drop_table(p_tablename), 'drop table ' || p_tablename || ' cascade constraints');
   	ELSE
	  execute_if_true(is_drop_table(p_tablename), 'drop table ' || p_tablename);
  	END IF;
  END drop_table;
  
  PROCEDURE rename_table(p_oldtablename IN VARCHAR2, p_newtablename IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_table_exists(p_oldtablename), 'alter table ' || p_oldtablename || ' rename to ' || p_newtablename);
  END rename_table;
  
  PROCEDURE add_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2, p_columntype IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_add_column(p_tablename, p_columnname), 'alter table ' || p_tablename || ' add ' || p_columnname || ' ' || p_columntype);
  END add_column;
  
  PROCEDURE drop_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_drop_column(p_tablename, p_columnname), 'alter table ' || p_tablename || ' drop column ' || p_columnname);  
  END;

  PROCEDURE force_drop_column(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2)
  IS
    m_flag BOOLEAN;
  BEGIN
  	m_flag := is_drop_column(p_tablename, p_columnname);
  	IF m_flag THEN
	    set_column_null(p_tablename, p_columnname);
	    execute_command('update ' || p_tablename || ' set ' || p_columnname || ' = null where ' || p_columnname || ' is not null');
	    commit;
	  	drop_column(p_tablename, p_columnname);
  	END IF;
  END;
  
  PROCEDURE add_fk(p_tablename IN VARCHAR2, p_fkname IN VARCHAR2, p_columnname IN VARCHAR2, p_referencetablename IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_add_fk(p_tablename, p_fkname), 'alter table ' || p_tablename || ' add constraint ' || p_fkname || ' foreign key (' || p_columnname || ') references ' || p_referencetablename);
  END add_fk;
  PROCEDURE create_pk(p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(not(is_pk_exists(p_tablename)), 'alter table ' || p_tablename || ' add primary key ('|| p_columnlist || ')');
  END create_pk;
  
  PROCEDURE drop_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_drop_constraint(p_tablename, p_constraintname), 'alter table ' || p_tablename || ' drop constraint ' || p_constraintname);
  END drop_constraint;

  PROCEDURE create_constraint(p_tablename IN VARCHAR2, p_constraintname IN VARCHAR2, p_constraintSql IN VARCHAR2) 
  IS
  BEGIN
    execute_if_true(is_create_constraint(p_tablename, p_constraintname), 'alter table ' || p_tablename || ' add constraint ' || p_constraintname || ' ' || p_constraintSql);
  END create_constraint;
  
  PROCEDURE create_sequence(p_sequencename IN VARCHAR2)
  IS
  BEGIN
	  execute_if_true(not(is_sequence_exists(p_sequencename)), 'create sequence  ' || p_sequencename);
  END create_sequence;

  PROCEDURE drop_sequence(p_sequencename IN VARCHAR2)
  IS
  BEGIN
	  execute_if_true(is_sequence_exists(p_sequencename), 'drop sequence  ' || p_sequencename);
  END drop_sequence;
  
  PROCEDURE rename_constraint(p_tablename IN VARCHAR2, p_oldname IN VARCHAR2, p_newname IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_constraint_exists(p_tablename, p_oldname), 'alter table ' || p_tablename || ' rename constraint ' || p_oldname || ' TO ' || p_newname);
  END rename_constraint;

  PROCEDURE set_column_notnull(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_column_null(p_tablename, p_columnname), 'alter table ' || p_tablename || ' modify(' || p_columnname || ' NOT NULL)');
  END set_column_notnull;
  
  PROCEDURE set_column_null(p_tablename IN VARCHAR2, p_columnname IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_column_notnull(p_tablename, p_columnname), 'alter table ' || p_tablename || ' modify(' || p_columnname || ' NULL)');
  END set_column_null;
  
  PROCEDURE rename_column(p_tablename IN VARCHAR2, p_from_columnname IN VARCHAR2, p_to_columnname IN VARCHAR2)
  IS
  BEGIN
    execute_if_true(is_column_exists(p_tablename, p_from_columnname), 'alter table ' || p_tablename || ' rename column ' || p_from_columnname || ' TO ' || p_to_columnname);
  END rename_column;

  PROCEDURE create_index(p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE)
  IS
  BEGIN
  	create_index_of_type(NULL, p_indexname, p_tablename, p_columnlist, p_tablespace);
  END create_index;

  PROCEDURE create_bitmap_index(p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE)
  IS
  BEGIN
  	create_index_of_type('bitmap', p_indexname, p_tablename, p_columnlist, p_tablespace);
  END create_bitmap_index;
  
  PROCEDURE create_unique_index(p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE)
  IS
  BEGIN
  	create_index_of_type('unique', p_indexname, p_tablename, p_columnlist, p_tablespace);
  END create_unique_index;

  PROCEDURE create_index_of_type(p_indextype IN VARCHAR2, p_indexname IN VARCHAR2, p_tablename IN VARCHAR2, p_columnlist IN VARCHAR2, p_tablespace IN VARCHAR2 := C_INDEX_TABLESPACE)
  IS
    m_indextype VARCHAR2(255 BYTE);
  BEGIN
    -- p_indextype should be one of 'bitmap' 'unique' NULL'
    m_indextype := p_indextype;
    IF m_indextype = NULL THEN
      m_indextype := ' ';
    END IF;  
  	execute_if_true(is_create_index(p_indexname), 'create ' || m_indextype || ' index ' || p_indexname || ' on ' || p_tablename || ' ( ' || p_columnlist || ' ) ' || get_tablespace_command( p_tablespace ));
  END create_index_of_type;

  PROCEDURE drop_index(p_indexname IN VARCHAR2)
  IS
  BEGIN
  	execute_if_true(is_drop_index(p_indexname), 'drop index ' || p_indexname);
  END drop_index;

  PROCEDURE drop_procedure(p_procedurename IN VARCHAR2)
  IS
  BEGIN
  	execute_if_true(is_procedure_exists(p_procedurename), 'drop procedure ' || p_procedurename);
  END drop_procedure;

  PROCEDURE grant_table_access(p_tablename IN VARCHAR2, p_username IN VARCHAR2, p_select IN BOOLEAN := TRUE, p_insert IN BOOLEAN := TRUE, p_update IN BOOLEAN := TRUE, p_delete IN BOOLEAN := TRUE)
  IS
  BEGIN
  	IF is_user_exists(p_username) THEN
  		execute_if_true(p_select, 'GRANT SELECT ON ' || p_tablename || ' TO ' || p_username);
  		execute_if_true(p_insert, 'GRANT INSERT ON ' || p_tablename || ' TO ' || p_username);
  		execute_if_true(p_update, 'GRANT UPDATE ON ' || p_tablename || ' TO ' || p_username);
  		execute_if_true(p_delete, 'GRANT DELETE ON ' || p_tablename || ' TO ' || p_username);
  	ELSE
	    DBMS_OUTPUT.PUT_LINE('WARNING CANNOT GRANT RIGHTS AS USER DOES NOT EXIST - ' || p_username);
  	END IF;
  END grant_table_access;

  PROCEDURE grant_command(p_objectname IN VARCHAR2, p_command IN VARCHAR2, p_username IN VARCHAR2)
  IS
  BEGIN
  	IF is_user_exists(p_username) THEN
	    execute_command('GRANT ' || p_command || ' ON ' || p_objectname || ' TO ' || p_username);
	ELSE
	    DBMS_OUTPUT.PUT_LINE('WARNING CANNOT GRANT RIGHTS AS USER DOES NOT EXIST - ' || p_username);
	END IF;
  END grant_command;

  PROCEDURE grant_execute(p_objectname IN VARCHAR2, p_username IN VARCHAR2)
  IS
  BEGIN
  	grant_command(p_objectname, 'EXECUTE', p_username);
  END grant_execute;

END schema_support;
/
